"""
starboard embed extractors
"""

from discord import Embed


class NoEmbed(Exception):
    """Signaler for when there are no extractable embeds."""

    pass


def twitter_extractor(_message, tw_embed, embed):
    """Extractor for twitter embeds."""

    # step 1: prefix author to description, then append the original
    # embed description, which usually is the original twitter post's
    # text
    tw_author = tw_embed.author.name
    url_prefix = f"<:twitter:589310733846577153> [{tw_author}]({tw_embed.url})"
    embed.description = f"**{url_prefix}**\n{tw_embed.description}"

    # step 2: images, which are just a copy of the original, and should
    # suffice.
    if tw_embed.image.url != Embed.Empty:
        embed.set_image(url=tw_embed.image.url)

    # step 3: video copying if any. we only give a url back to the
    # original twitter video because we can't set videos ourselves.
    if tw_embed.video.url != Embed.Empty:
        embed.description += f"\n\n[video link]({tw_embed.video.url})"


def image_embed_extractor(_message, img_embed, embed):
    """Extracts an image from any embed and inserts that into
    our starboard embed.

    This is *supposedly* more reliable then running our own regexes, also,
    faster, than running our own regexes, as the data is already there.

    This does not work on extracting gfycat/giphy stuff, as they use a rich
    embed, with the 'video' parameter. Discord API does not allow usage of
    that field.

    Also extracts description when available.
    """
    img_url = img_embed.image.url

    if img_url == Embed.Empty:
        raise NoEmbed()

    if img_embed.description:
        embed.description += img_embed.description

    embed.set_image(url=img_url)


EXTRACTORS = {"https://twitter.com": twitter_extractor, "": image_embed_extractor}


def try_extract_embed(message, em: Embed):
    """Go through available extractors. Raises NoEmbed if no extractor
    was found or no embeds are available."""
    embeds = message.embeds

    if not embeds:
        raise NoEmbed()

    for embed in embeds:
        url = embed.url

        # skip embeds without URLs
        if url == Embed.Empty:
            continue

        for processor_tag, processor in EXTRACTORS.items():
            if url.startswith(processor_tag):
                # a match was found for a given processor, hopefully
                # it matches correctly
                try:
                    processor(message, embed, em)
                    return
                except NoEmbed:
                    pass

    raise NoEmbed()
