import logging
import re
import random
import urllib.parse
import collections
import time
import json
import timeit
import io
from typing import Optional

import discord

from discord.ext import commands

from .common import Cog, SayException
from .eval import Code

log = logging.getLogger(__name__)

RXKCD_ENDPOINT = "http://localhost:3000/search"


class Extra(Cog, requires=["config"]):
    """Extra commands that don't fit in any other cogs."""

    def __init__(self, bot):
        super().__init__(bot)

        self.socket_stats = collections.Counter()
        self.sock_start = time.monotonic()

    @Cog.listener()
    async def on_socket_response(self, data):
        self.socket_stats[data.get("t")] += 1

    @commands.command()
    async def avatar(self, ctx, *, person: discord.User = None):
        """Get someone's avatar."""
        if person is None:
            person = ctx.author

        url = person.avatar_url_as(static_format="png")
        await ctx.send(url)

    @commands.command()
    async def xkcd(self, ctx, number: str = ""):
        """Get XKCD shit."""

        await self.jcoin.pricing(ctx, self.prices["API"])
        info = None

        with ctx.typing():
            url = "https://xkcd.com/info.0.json"
            info = await self.get_json(url)

            try:
                as_int = int(number)
            except (TypeError, ValueError):
                as_int = -1

            if number == "rand":
                if info is None:
                    raise self.SayException("failed to fetch xkcd info.0.json")

                random_num = random.randint(0, info["num"])
                info = await self.get_json(
                    f"https://xkcd.com/{random_num}/" "info.0.json"
                )
            elif as_int > 0:
                info = await self.get_json(f"https://xkcd.com/{number}/" "info.0.json")

        if info is None:
            raise self.SayException("failed to fetch xkcd")

        await ctx.send(f'xkcd {info["num"]} => {info["img"]}')

    async def _do_rxkcd(self, ctx, terms):
        async with self.bot.session.post(RXKCD_ENDPOINT, json={"search": terms}) as r:
            if r.status != 200:
                raise self.SayException(f"Got a not good error code: {r.code}")

            data = await r.text()
            data = json.loads(data)
            if not data["success"]:
                raise self.SayException(
                    "XKCD retrieval failed:" f' {data["message"]!r}'
                )

            if len(data["results"]) < 1:
                raise self.SayException("No comics found.")

            comic = data["results"][0]

            em = discord.Embed(title=f"Relevant XKCD for `{terms!r}`")
            em.description = f'XKCD {comic["number"]}, {comic["title"]}'
            em.set_image(url=comic["image_url"])

            await ctx.send(embed=em)

    async def _do_rxkcd_debug(self, ctx):
        async with self.bot.session.post(
            RXKCD_ENDPOINT, json={"search": "standards"}
        ) as r:
            await ctx.send(repr(r))
            await ctx.send(await r.text())

    @commands.command()
    async def rxkcd(self, ctx, *, terms: str):
        """Get a Relevant XKCD.

        Made with https://github.com/adtac/relevant-xkcd
        """
        try:
            if "-debug" in terms:
                return await self._do_rxkcd_debug(ctx)

            async with ctx.typing():
                await self._do_rxkcd(ctx, terms)
        except Exception as err:
            raise self.SayException(f"Error while requesting data: `{err!r}`")

    @commands.command(aliases=["yt"])
    async def youtube(self, ctx, *, search_term: str):
        """Search youtube."""
        log.info(f"[yt] {ctx.author} {search_term!r}")

        with ctx.typing():
            query_string = urllib.parse.urlencode({"search_query": search_term})
            url = f"http://www.youtube.com/results?{query_string}"
            await self.jcoin.pricing(ctx, self.prices["OPR"])

            html_content = await self.http_get(url)
            future_re = self.loop.run_in_executor(
                None, re.findall, r"href=\"\/watch\?v=(.{11})", html_content
            )

            search_results = await future_re

        if len(search_results) < 2:
            await ctx.send("!yt: No results found.")
            return

        await ctx.send(f"http://www.youtube.com/watch?v={search_results[0]}")

    @commands.command()
    async def sockstats(self, ctx):
        """Event count through the websocket."""
        delta = time.monotonic() - self.sock_start
        minutes = delta / 60
        total = sum(self.socket_stats.values())
        events_minute = round(total / minutes, 2)

        await ctx.send(
            f"{total} events, {events_minute}/minute:" f"\n{self.socket_stats}"
        )

    @commands.command(hidden=True)
    async def awoo(self, ctx):
        """A weeb made me do this."""
        await ctx.send(
            "https://cdn.discordapp.com/attachments/"
            "400895061660794880/469176104167407627/awoo.gif"
        )

    @commands.command()
    @commands.guild_only()
    async def presence(self, ctx, member: discord.Member = None):
        """Shows your status/presence info in José's view."""
        if member is None:
            member = ctx.member

        status = member.status
        try:
            game_name = member.game.name
        except AttributeError:
            game_name = "<no game>"

        game_name = game_name.replace("@", "@\u200b")

        game_name = await commands.clean_content().convert(ctx, game_name)
        await ctx.send(f"status: `{status}`, game: `{game_name}`")

    @commands.command()
    @commands.is_owner()
    async def elixir(self, ctx, *, terms: str):
        """Search through the Elixir documentation.

        Powered by https://github.com/lnmds/elixir-docsearch.
        """
        await ctx.trigger_typing()
        try:
            base = self.bot.config.elixir_docsearch
        except AttributeError:
            raise self.SayException(
                "No URL for elixir-docsearch" " found in configuration."
            )

        async with self.bot.session.get(
            f"http://{base}/search", json={"query": terms}
        ) as r:
            if r.status != 200:
                raise self.SayException(f"{r.status} is not 200, rip.")

            res = await r.json()
            if len(res) == 0:
                raise self.SayException("No results found.")

            res = res[:5]
            em = discord.Embed(colour=discord.Colour.blurple())
            em.description = ""
            for (entry, score) in res:
                name = entry.split("/")[-1].replace(".html", "")
                em.description += (
                    f"[{name}](https://hexdocs.pm{entry}) " f"({score * 100}%)\n"
                )

            await ctx.send(embed=em)

    @commands.command()
    async def unfurl(self, ctx, message_id: int, channel_id: Optional[int] = None):
        """Get the raw contents of a message, without spoilers, etc."""
        channel_id = channel_id or ctx.channel.id
        channel = self.bot.get_channel(channel_id)

        if channel is None:
            raise SayException("unknown channel")

        try:
            message = await channel.fetch_message(message_id)
        except discord.errors.NotFound:
            raise SayException("message not found")

        content = self.bot.clean_content(message.content)

        em = discord.Embed(title=f"unfurled message by {message.author}")
        em.description = f"```{content}```"

        await ctx.send(embed=em)

    @commands.command()
    @commands.is_owner()
    async def timeit(self, ctx, stmt: Code, setup_stmt_param: Optional[Code] = None):
        """Python's timeit as a command."""
        setup_stmt: str = setup_stmt_param or "pass"
        timer = timeit.Timer(stmt, setup_stmt)

        stream = io.StringIO()
        number, _ = timer.autorange()

        try:
            secs = await self.loop.run_in_executor(None, timer.timeit)
            await ctx.send(f"1000000 steps: {round(secs, 9)} secs/step")
        except:
            timer.print_exc(file=stream)
            return await ctx.send(stream.getvalue())


def setup(bot):
    bot.add_cog(Extra(bot))
