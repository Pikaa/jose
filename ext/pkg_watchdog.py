import asyncio
import logging

import discord
import feedparser

from asyncpg import UniqueViolationError
from discord.ext import commands

from .common import Cog

log = logging.getLogger(__name__)

ARCH_RSS_URL = "https://www.archlinux.org/feeds/packages/"

CHANGELOGS = {
    "wine": "https://www.winehq.org/announce/{version}",
    "syncthing": "https://github.com/syncthing/syncthing/" "releases/tag/v{version}",
    "riot-web": "https://github.com/vector-im/riot-web/" "releases/tag/v{version}",
    "riot-desktop": "https://github.com/vector-im/riot-web/" "releases/tag/v{version}",
    "mesa": "https://www.mesa3d.org/relnotes/{version}.html",
    "lutris": "https://github.com/lutris/lutris/releases/tag/v{version}",
    # going with the 1.x branch for now
    "libuv": "https://github.com/libuv/libuv/blob/v1.x/ChangeLog",
    "git": "https://github.com/git/git/"
    "blob/master/Documentation/RelNotes/{version}.txt",
    "asar": "https://github.com/electron/asar/blob/master/CHANGELOG.md",
    "qutebrowser": "https://github.com/qutebrowser/qutebrowser/"
    "releases/tag/v{version}",
    "brotli": "https://github.com/google/brotli/releases/tag/v{version}",
    "calibre": "https://calibre-ebook.com/whats-new",
    "sqlite3": "https://www.sqlite.org/news.html",
    "openssl": "https://github.com/openssl/openssl/blob/master/NEWS",
    # uwu
    "crystal": "https://github.com/crystal-lang/crystal/releases/tag/{version}",
    "zig": "https://ziglang.org/download/{version}/release-notes.html",
    "python": "https://docs.python.org/{version}/whatsnew/changelog.html",
}



def _changelog(package: str, version: str) -> str:
    changelog_url = CHANGELOGS.get(package)
    changelog_url = "" if changelog_url is None else changelog_url
    changelog_url = changelog_url.format(version=version)
    return changelog_url

def _real_python_ver(version: str) -> str:
    return version.replace(version[-2:-1], '')

class PackageWatchdog(Cog):
    """A cog that sends a message in a channel for new package updates.

    You can select which packages you want updates from.

    The source is the Arch Linux Package RSS, found here:
    https://www.archlinux.org/feeds/packages/
    """

    def __init__(self, bot):
        super().__init__(bot)
        self.rss_task = self.loop.create_task(self.poller())

    def cog_unload(self):
        self.rss_task.cancel()

    async def poller(self):
        """Main poller task"""
        try:
            while True:
                await self.poll()

                # 30min poll time
                await asyncio.sleep(1800)
        except:
            log.exception("poll pkg watchdog task fail")

    async def poll(self):
        """Poll the Arch Linux Package RSS"""
        async with self.bot.session.get(ARCH_RSS_URL) as resp:
            assert resp.status == 200
            raw_rss = await resp.text()
            feed = feedparser.parse(raw_rss)

        await self.process_feed(feed)

    async def process_feed(self, feed: dict):
        """Process the RSS feed information and
        dispatch to channels."""

        log.info(f"Working {len(feed.entries)} entries")

        for entry in feed.entries:
            title = entry.title
            package, version, _arch = title.split()
            actual_version, _rev = version.split("-")
            if package == 'python':
               version = _real_python_ver(actual_version)

            old_vers = await self.pool.fetchval(
                """
            SELECT version FROM pw_packages
            WHERE package = $1
            """,
                package,
            )

            if old_vers is None:
                log.info(f"new package: {package} {actual_version}")

                await self.pool.execute(
                    """
                INSERT INTO pw_packages (package, version)
                VALUES ($1, $2)
                """,
                    package,
                    actual_version,
                )

                old_vers = actual_version
            else:
                log.info(f"package: {package} {actual_version}")

                await self.pool.execute(
                    """
                UPDATE pw_packages
                SET version = $1
                WHERE package = $2
                """,
                    actual_version,
                    package,
                )

            if old_vers == actual_version:
                continue

            log.info(f"package UPDATE: {package} {actual_version}")

            # an update!
            await self.dispatch_update(package, actual_version)

    async def dispatch_update(self, package: str, actual_version: str):
        """Dispatch the package update to the guilds
        that are subscribed to it."""
        changelog_url = _changelog(package, actual_version)

        announcement = (
            f"\N{ELECTRIC TORCH} **{package} {actual_version}**\n" f"{changelog_url}"
        )

        # get all channels that are linked to this package

        channels = await self.pool.fetch(
            """
        SELECT channel_id
        FROM pw_channels
        JOIN pw_guild_packages
        ON pw_guild_packages.guild_id = pw_channels.guild_id
        WHERE pw_guild_packages.package = $1
        """,
            package,
        )

        channels = [r["channel_id"] for r in channels]

        for chan_id in channels:
            chan = self.bot.get_channel(chan_id)

            if not chan:
                continue

            await chan.send(announcement)

    @commands.group(aliases=["pw"], invoke_without_command=True)
    async def pkgwatch(self, ctx):
        """Package watchdog.

        By default gives you the current status of the watchdog
        """
        chan_id = await self.pool.fetchval(
            """
        SELECT channel_id FROM pw_channels
        WHERE guild_id = $1
        """,
            ctx.guild.id,
        )

        if not chan_id:
            return await ctx.send("Package Watchdog is not enabled " "in this guild.")

        chan = self.bot.get_channel(chan_id)

        if not chan:
            return await ctx.send(
                "Package Watchdog is set to a channel " "that José can't see"
            )

        pkgs = await self.pool.fetch(
            """
        SELECT package
        FROM pw_guild_packages
        WHERE guild_id = $1
        """,
            ctx.guild.id,
        )

        pkgs = [r["package"] for r in pkgs]
        pkgs = "<none>" if not pkgs else ", ".join(pkgs)

        await ctx.send(
            "Package Watchdog is enabled and reporting "
            f"to {chan.mention}.\n"
            f"Packages being watched: {pkgs}"
        )

    @pkgwatch.command()
    @commands.has_permissions(manage_guild=True)
    async def register(self, ctx, channel: discord.TextChannel):
        """Register the guild to the watchdog."""
        try:
            await self.pool.execute(
                """
            INSERT INTO pw_channels (guild_id, channel_id)
            VALUES ($1, $2)
            """,
                ctx.guild.id,
                channel.id,
            )

            await ctx.ok()
        except UniqueViolationError:
            await self.pool.execute(
                """
            UPDATE pw_channels
            SET channel_id = $1
            WHERE guild_id = $2
            """,
                channel.id,
                ctx.guild.id,
            )

            await ctx.send(f"Updated channel to {channel.mention}")

    @pkgwatch.command()
    @commands.has_permissions(manage_guild=True)
    async def unregister(self, ctx):
        """Unregister your guild from the watchdog."""
        await self.pool.execute(
            """
        DELETE FROM pw_channels
        WHERE guild_id = $1
        """,
            ctx.guild.id,
        )

        await ctx.ok()

    async def _ensure_register(self, ctx):
        chan_id = await self.pool.fetchval(
            """
        SELECT channel_id FROM pw_channels
        WHERE guild_id = $1
        """,
            ctx.guild.id,
        )

        if not chan_id:
            raise self.SayException(
                "Package Watchdog is not configured. "
                f"Look over `{ctx.prefix}help pw`."
            )

    @pkgwatch.command(name="addpkg")
    @commands.has_permissions(manage_guild=True)
    async def add_package(self, ctx, package: str):
        """Add a package to watch.

        Keep in mind this does not check
        if the package already exist in the Arch Linux
        repositories.
        """
        await self._ensure_register(ctx)
        package = package.lower()

        try:
            await self.pool.execute(
                """
            INSERT INTO pw_guild_packages (guild_id, package)
            VALUES ($1, $2)
            """,
                ctx.guild.id,
                package,
            )
        except UniqueViolationError:
            await ctx.send("Package is already on the guild's watchdog")
            return

        await ctx.ok()

    @pkgwatch.command(name="rmpkg")
    @commands.has_permissions(manage_guild=True)
    async def remove_package(self, ctx, package: str):
        """Remove a package from the watchdog for your guild."""
        await self._ensure_register(ctx)
        package = package.lower()

        res = await self.pool.execute(
            """
        DELETE FROM pw_guild_packages
        WHERE guild_id = $1 AND package = $2
        """,
            ctx.guild.id,
            package,
        )

        if res == "DELETE 0":
            return await ctx.send(f"This package is not being watched.")

        await ctx.ok()

    @pkgwatch.command(name="info")
    async def pkg_info(self, ctx, package: str):
        """Fetch basic info on a package."""
        version = await self.pool.fetchval(
            """
        SELECT version FROM pw_packages
        WHERE package = $1
        """,
            package,
        )
        version = "<not found>" if not version else version
        changelog = _changelog(package, version)

        await ctx.send(f"Version: {version}\n" f"Changelog URL: {changelog}")


def setup(bot):
    bot.add_jose_cog(PackageWatchdog)
