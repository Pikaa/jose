import json
import enum
from typing import List, Optional
from pathlib import Path

import discord
from discord.ext import commands

from .common import Cog, shell
from .eval import codeblock, Code
from .utils import Timer

CAN_ZIG = [
    162819866682851329,  # lun
    186532514163064842,  # belawl
    97104885337575424,  # sloc
]


def is_in_list(user_ids: List[int]):
    """Return a command check that checks if the message author is in a
    given user id list."""

    async def _check(ctx):
        return ctx.author.id in user_ids

    return _check


class ZigMode(enum.Enum):
    BUILD_EXE = "build-exe"
    TEST = "test"


class ExtraLang(Cog):
    """V playground and (owner-only) zig command."""

    VLANG_ENDPOINT = "https://play.vlang.io/compile"
    ZIG_FILE = Path("./eval.zig")
    ZIG_DOWNLOAD_INDEX = "https://ziglang.org/download/index.json"

    async def execute_v(self, code):
        """Execute V code."""
        payload = {"body": code}

        async with self.bot.session.post(self.VLANG_ENDPOINT, data=payload) as resp:
            if resp.status != 200:
                raise self.SayException("Failed to contact the playground")

            return await resp.text()

    @commands.command(aliases=["v"])
    @commands.cooldown(1, 5, commands.BucketType.default)
    async def veval(self, ctx, *, code: Code(strip_ticks=True)):
        """Execute V code.

        https://vlang.io."""
        resp = await self.execute_v(code.lstrip("\n"))
        in_code = codeblock(code, lang="go")

        # vals[0] contains stdout, vals[1] contains vfmt output
        vals = resp.split("==vmft:")
        stdout = codeblock(vals[0] or "<no stdout>")
        await ctx.send(f"input: {in_code}\noutput: {stdout}")

    @commands.group(aliases=["zigeval"], invoke_without_command=True)
    @commands.check(is_in_list(CAN_ZIG))
    async def zig(
        self,
        ctx,
        mode: Optional[ZigMode] = ZigMode.BUILD_EXE,
        *,
        code: Code(strip_ticks=True) = None,
    ):
        """Execute Zig.

        available modes: build-exe (default), test.

        https://ziglang.org.
        """
        if mode is None:
            raise self.SayException("invalid mode")

        if not code:
            raise self.SayException("missing code for mode that requires code")

        with self.ZIG_FILE.open("w") as fd:
            fd.write(code)

        args = "--library c"

        with ctx.typing():
            with Timer() as compile_timer:
                compiler_out = await shell(f"zig {mode.value} {self.ZIG_FILE} {args}")

        try:
            out = await shell("./eval")
        except Exception as err:
            out = f"Failed to run program: {err!r}"

        final_out = compiler_out if compiler_out else out

        # id say its close enough lamo....
        final_out = codeblock(final_out, lang="rust")
        await ctx.send(f"{final_out}\ncompilation took {compile_timer}")

    @zig.command(name="version")
    async def zig_version(self, ctx):
        """Get current version of master and stable zig."""
        async with self.bot.session.get(self.ZIG_DOWNLOAD_INDEX) as resp:
            if resp.status != 200:
                raise self.SayException("Failed to fetch zig download index.json")

            index = json.loads(await resp.text())

        master = index["master"]

        keys = list(index.keys())
        keys.remove("master")
        max_stable = max(keys)
        stable = index[max_stable]

        local_zig_ver = await shell("zig version")

        em = discord.Embed(
            title="Zig versions", description="", color=discord.Color(0xF7A41D)
        )

        em.description += f"(running on jose: {local_zig_ver})\n"
        em.description += f'[master]({master["docs"]}) at {master["date"]}\n'
        em.description += (
            f'[stable]({stable["docs"]}) ({max_stable}) at ' f'{stable["date"]}'
        )

        await ctx.send(embed=em)

    @zig.command(name="search")
    @commands.check(is_in_list(CAN_ZIG))
    async def zig_search(self, ctx, *, search_terms: str):
        out = await shell(
            f"zig-docsearch ~/.local/share/zigdoc.bin search {search_terms}"
        )
        em = discord.Embed(
            title="zig stdlib search", description="", color=discord.Color(0xF7A41D)
        )

        els = out.split("\n---\n")
        for el in els[:5]:
            spl = el.split(":")
            ns = spl[0]

            if len(spl) > 1:
                em.add_field(name=ns, value=spl[1], inline=True)
            else:
                em.add_field(name=ns, value="<nodoc>", inline=True)

        if not els:
            em.description = "<not found>"

        await ctx.send(embed=em)


def setup(bot):
    bot.add_jose_cog(ExtraLang)
