module.exports = {
    apps: [
        {
            name: "josecoin",
            script: "jcoin/josecoin.py",
            interpreter: "env/bin/python",
            restart_delay: 2000
        },
        {
            name: "jose",
            script: "jose.py",
            interpreter: "env/bin/python",
            restart_delay: 5000
        }
    ]
}
